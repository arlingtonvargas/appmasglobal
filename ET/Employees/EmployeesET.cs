﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ET.Employees
{
    public abstract class EmployeesET : EmployeeDTO
    {
        public abstract void CalculateAnnualSalary(); 
    }
}
