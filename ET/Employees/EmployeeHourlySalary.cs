﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ET.Employees
{
    public class EmployeeHourlySalary : EmployeesET
    {
        public EmployeeHourlySalary(EmployeeDTO e)
        {
            this.id = e.id;
            this.name = e.name;
            this.contractTypeName = e.contractTypeName;
            this.roleId = e.roleId;
            this.roleName = e.roleName;
            this.roleDescription = e.roleDescription;
            this.hourlySalary = e.hourlySalary;
            this.monthlySalary = e.monthlySalary;
            this.annualSalaries = 0;

            this.CalculateAnnualSalary();
        }

        public override void CalculateAnnualSalary()
        {
            this.annualSalaries = 120 * this.hourlySalary * 12;
        }

    }
}
