﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ET.Employees
{
    public class EmployeeFactoryImp: EmployeeFactory
    {
        public EmployeesET MakeEmployee(EmployeeDTO e){

            switch (e.contractTypeName)
	        {
                case Constanst.Constanst.HOURLY_SALARY_EMPLOYEE:
                    return new EmployeeHourlySalary(e);

                case Constanst.Constanst.MONTHLY_SALARY_EMPLOYEE:
                    return new EmployeeMonthlySalary(e);                    

		        default:
                    return null;
	        }

        }
    }
}
