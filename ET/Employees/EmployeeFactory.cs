﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ET.Employees
{
    public interface EmployeeFactory
    {
        EmployeesET MakeEmployee(EmployeeDTO e);
    }
}
