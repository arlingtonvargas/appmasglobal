﻿using BL.Employees;
using ET.Employees;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AppMasGlobal.Controllers
{
    public class EmployeesController : ApiController
    {
        EmployeesBL mEmployeesBL = new EmployeesBL();
        // GET: api/Employees
        public async Task<EmployeesDTOListET> Get()
        {
            return await mEmployeesBL.GetAllEmployees();
        }

        // GET: api/Employees/5
        public async Task<EmployeeDTO> Get(int id)
        {
            return await mEmployeesBL.GetEmployeById(id);
        }

       
    }
}
