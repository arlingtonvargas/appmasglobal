﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using ET.Employees;

namespace DA.Employees
{
    public class EmployeesDA
    {
        static HttpClient mClient = new HttpClient();
        const string mUrlComplement = "Employees/";
        EmployeeFactoryImp wEmployeeFact = new EmployeeFactoryImp();

        
        public async Task<EmployeeDTO> GetEmployeeById(int pIdEmployee)
        {
            try
            {
                EmployeesDTOListET wEmployessList = new EmployeesDTOListET();
                EmployeeDTO wEmployeeRes = null;

                string wUrlFinal = string.Format("{0}{1}", Constants.Constanst.URL_API, mUrlComplement);
                HttpResponseMessage response = await mClient.GetAsync(wUrlFinal);
                if (response.IsSuccessStatusCode)
                {
                    wEmployessList = await response.Content.ReadAsAsync<EmployeesDTOListET>();
                }

                wEmployeeRes = wEmployessList.Where(x => x.id == pIdEmployee).FirstOrDefault();

                wEmployeeRes = wEmployeeFact.MakeEmployee(wEmployeeRes);

                return wEmployeeRes;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<EmployeesDTOListET> GetAllEmployees()
        {
            try
            {
                EmployeesDTOListET wEmployessList = new EmployeesDTOListET();
                EmployeesDTOListET wEmployessListRes = new EmployeesDTOListET();

                string wUrlFinal = string.Format("{0}{1}", Constants.Constanst.URL_API, mUrlComplement);
                HttpResponseMessage response = await mClient.GetAsync(wUrlFinal);
                if (response.IsSuccessStatusCode)
                {
                    wEmployessList = await response.Content.ReadAsAsync<EmployeesDTOListET>();
                }

                wEmployessList.ForEach(x => wEmployessListRes.Add(wEmployeeFact.MakeEmployee(x)));
                   

                return wEmployessListRes;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}