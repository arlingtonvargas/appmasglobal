﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DA.Employees;
using ET.Employees;

namespace BL.Employees
{
    public class EmployeesBL
    {
        EmployeesDA mEmployeesDA = new EmployeesDA();
        public async Task<EmployeeDTO> GetEmployeById(int pIdEmployee)
        {
            try
            {
                return await mEmployeesDA.GetEmployeeById(pIdEmployee);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<EmployeesDTOListET> GetAllEmployees()
        {
            try
            {
                return await mEmployeesDA.GetAllEmployees();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
